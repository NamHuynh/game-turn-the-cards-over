let inputRows = null;
let inputCols = null;
let btnSetup = null;
let rows = null;
let cols = null;
let array = [];
let guess = null;
let pause = null;
// card
let templateCards = document.getElementById('card');

(function init() {
  // panel
  inputRows = document.getElementById('input-rows');
  inputCols = document.getElementById('input-cols');
  btnSetup = document.getElementById('btn-setup');
  // > panel
  pause = false;

  // card
  // panel
  btnSetup.addEventListener('click', ()=>{
    rows = inputRows.value;
    cols = inputCols.value;
    setArray(rows, cols);
    setRowsItem (rows,cols, shuffle (array));
  });
  // > panel
})();

// gameViewer
function setRowsItem (rows, cols, arrayValue){
  let listCard = document.getElementById('list-card');
  let game = document.getElementById('game');
  let wrapperGame = null;
  let counter = 0;

  if ( game.children.length !== 0 ){
    for (let i = game.children.length - 1 ; i >= 0  ; i--) {
      game.children[i].remove();
    }
  }
  for (let i = 1 ; i <= rows ; i++ ){
    wrapperGame = listCard.cloneNode(true);

    for (let j = 1 ; j <= cols ; j++ ){

      // card
      let card = templateCards.cloneNode(true);
      card.querySelector('.inside .front #value').innerHTML = arrayValue[counter];
      card.setAttribute('data-id',Math.random().toString(36).substring(7));
      card.addEventListener('click', ()=> {
        clickCard (card, rows * cols);
      });
      card.removeAttribute('style');
      // > card
      wrapperGame.removeAttribute('style');
      wrapperGame.appendChild(card);
      counter++;
    }
    game.appendChild(wrapperGame);
  }
}
// > gameViewer

// card
function clickCard (card , lengthCard){
  let inside = card.querySelector('.inside');
  if(!pause && !inside.classList.contains('picked') && !inside.classList.contains('matched')){
    inside.classList.add('picked');
    let value = card.querySelector('.inside .front #value').textContent;
    if(!guess){
      guess = value;
    }
    else if(guess === value  && inside.classList.contains('picked')){
      let picked = document.querySelectorAll('.picked');
      for( let i = 0 ; i < picked.length ; i++ ){
        picked[i].classList.add('matched');
      }
      guess = null;
    }
    else{
      guess = null;
      pause = true;
      setTimeout(function(){
        let picked = document.querySelectorAll('.picked');
        for( let i = 0 ; i < picked.length ; i++ ){
          picked[i].classList.remove('picked');
        }
        pause = false;
      }, 600);
    }
  }
  if(lengthCard % 2 === 0){
    if(document.querySelectorAll('.matched').length === lengthCard){
      let templateModal = document.getElementById('modal');
      templateModal.removeAttribute('style');
      document.getElementById('btn-restart').addEventListener('click', ()=>{
        templateModal.setAttribute('style','display: none');
        reset();
      });
    }
  }
  else{
    if(document.querySelectorAll('.matched').length === lengthCard - 1){
      for(let i = 0 ; i < document.querySelectorAll('.inside').length ; i++){
        let insides = document.querySelectorAll('.inside')[i];
        insides.classList.add('picked');
        insides.classList.add('matched');
      }

      let templateModal = document.getElementById('modal');
      templateModal.removeAttribute('style');
      document.getElementById('btn-restart').addEventListener('click', ()=>{
        templateModal.setAttribute('style','display: none');
        reset();
      });
    }
  }
}
// > card

// main
function reset (){
  setRowsItem(0,0,[]);
  inputRows.value = '';
  inputCols.value = '';
  for(let i = 0 ; i < document.querySelectorAll('.inside').length ; i++){
    let insides = document.querySelectorAll('.inside')[i];
    insides.classList.remove('picked');
    insides.classList.remove('matched');
  }
}
// < main

// panel
function setArray(rows, cols){
  array = [];
  let lengthItem = Math.ceil(( rows * cols ) / 2) ;
  if((rows * cols) % 2 === 0) {
    for (let i = 1; i <= lengthItem; i++){
      array.push(i);
    }
    array.push(...array);
  }
  else{
    for (let i = 1; i < lengthItem; i++){
      array.push(i);
    }
    array.push(...array);
    array.push(lengthItem);
  }
}

function shuffle (array){
  let counter = array.length, temp, index;
  while (counter > 0) {
    index = Math.floor(Math.random() * counter);
    counter--;
    temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }
  return array;
}
// > panel
