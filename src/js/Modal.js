// eslint-disable-next-line no-unused-vars
var Modal = function () {
  let btnRestart = null;
  let modalPanel = null;
  let panel = null;

  (function init() {
    btnRestart = document.getElementById('btn-restart');
    modalPanel = document.getElementById('modal');
    // eslint-disable-next-line no-undef
    panel = new Panel();
    btnRestart.addEventListener('click',() => {
      reset();
    });
  })();

  this.showPanelModal = function () {
    modalPanel.classList.add('show');
  };

  function hiddenPanelModal() {
    modalPanel.classList.remove('show');
  }

  function reset() {
    // eslint-disable-next-line no-undef
    let resetRow = new GameViewer();
    resetRow.setRowsItem(0,0,[]);
    panel.resetInput();
    hiddenPanelModal();
  }
};

