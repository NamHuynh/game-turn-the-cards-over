// eslint-disable-next-line no-unused-vars
var Panel = function () {
  let inputRows = null;
  let inputCols = null;
  let btnSetup = null;
  let array = [];
  var gameViewer = null;

  (function init() {
    inputRows = document.getElementById('input-rows');
    inputCols = document.getElementById('input-cols');
    btnSetup = document.getElementById('btn-setup');
    // eslint-disable-next-line no-undef
    gameViewer = new GameViewer();

    btnSetup.addEventListener('click', () => {
      if( Number.isInteger(Number(inputRows.value)) && Number.isInteger(Number(inputCols.value)) ){
        if (( inputCols.value <= 10 && inputRows.value <= 10 && inputRows.value * inputCols.value >= 4 )) {
          setArray(inputRows.value, inputCols.value);
          gameViewer.setRowsItem(inputRows.value , inputCols.value, shuffle (array));
        }
        else{
          gameViewer.setRowsItem(0,0,[]);
          inputRows.value = '';
          inputCols.value = '';
        }
      }
      else{
        gameViewer.setRowsItem(0,0,[]);
        inputRows.value = '';
        inputCols.value = '';
      }
    });
  })();

  this.resetInput = function () {
    inputRows.value = '';
    inputCols.value = '';
  };

  function setArray(rows, cols) {
    array = [];
    let lengthItem = Math.ceil((rows * cols) / 2);
    if ((rows * cols) % 2 === 0) {
      for (let i = 1; i <= lengthItem; i++) {
        array.push(i);
      }
      array.push(...array);
    } else {
      for (let i = 1; i < lengthItem; i++) {
        array.push(i);
      }
      array.push(...array);
      array.push(lengthItem);
    }
  }

  function shuffle(array) {
    let counter = array.length, temp, index;
    while (counter > 0) {
      index = Math.floor(Math.random() * counter);
      counter--;
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }
    return array;
  }
};

