// eslint-disable-next-line no-unused-vars
var GameViewer = function () {
  let game = null;

  (function init() {
    game = document.getElementById('game');
  })();
  // note setup check true false in this.

  this.setRowsItem = function(rows, cols, arrayValue) {
    let counter = 0;
    game.style.gridTemplateColumns = `repeat(${cols}, auto)`;
    if (game.children.length !== 0) {
      for (let i = game.children.length - 1; i >= 0; i--) {
        game.children[i].remove();
      }
    }
    for (let i = 1; i <= rows; i++) {
      for (let j = 1; j <= cols; j++) {
        // eslint-disable-next-line no-undef
        let card = new Card().createCard(arrayValue[counter]);
        card.onclick = ()=>{clickCard(card,rows * cols);};
        game.appendChild(card);
        counter++;
      }
    }
  };

  function clickCard (card, lengthCard){
    let inside = card.querySelector('.inside');
    if(!this.pause && !inside.classList.contains('picked') && !inside.classList.contains('matched')){
      inside.classList.add('picked');
      let value = card.querySelector('.inside .front').textContent;
      if(!this.guess){
        this.guess = value;
      }
      else if(this.guess === value  && inside.classList.contains('picked')){
        let nodePicked = document.querySelectorAll('.picked');
        for( let i = 0 ; i < nodePicked.length ; i++ ){
          nodePicked[i].classList.add('matched');
        }
        this.guess = null;
      }
      else{
        this.guess = null;
        this.pause = true;
        setTimeout(function(){
          let picked = document.querySelectorAll('.picked');
          for( let i = 0 ; i < picked.length ; i++ ){
            picked[i].classList.remove('picked');
          }
          this.pause = false;
        }, 600);
      }
      if(lengthCard % 2 === 0){
        if (document.querySelectorAll('.matched').length === lengthCard) {
          // eslint-disable-next-line no-unused-vars,no-undef
          let modal = new Modal().showPanelModal();
        }
      } else {
        if (document.querySelectorAll('.matched').length === lengthCard - 1) {
          for (let i = 0; i < document.querySelectorAll('.inside').length; i++) {
            let insides = document.querySelectorAll('.inside')[i];
            insides.classList.add('picked');
            insides.classList.add('matched');
          }
          // eslint-disable-next-line no-unused-vars,no-undef
          let modal = new Modal().showPanelModal();
        }
      }
    }
  }
};

